﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainForm.aspx.cs" Inherits="Scorpion.MainForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #Text1 {
            height: 135px;
            width: 542px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>
                Приветствуем в шифраторе!
            </h1>
        </div>

        <asp:TextBox ID="inputText" runat="server" Height="150px" style="resize:none; margin-bottom: 4px" placeholder="Здесь вы можете ввести текст, который хотите зашифровать/расшифровать" TextMode="MultiLine" Width="600px"></asp:TextBox>
        <br />
        <br />
        <asp:TextBox ID="inputKeyWord" runat="server" autocomplete="off" placeholder="Укажите ключ" Width="313px" Height="25px"></asp:TextBox>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <asp:Button ID="FileUploadButton" runat="server" Text="Загрузить из файла" />
&nbsp;
        <asp:FileUpload ID="FileUploader" runat="server" accept=".txt, .docx" style="display:none" 
            onchange="fileInfo()"/>
        <asp:Button ID="getFileButton" runat="server" Text="Button" OnClick="GetFileButton_Click" style="display:none" />

        <script type="text/javascript">
            function fileInfo() {
                document.getElementById("<%=getFileButton.ClientID %>").click();

            }
        </script>


        &nbsp;&nbsp;&nbsp;
        <asp:Label ID="ChoosenFilePath" runat="server" Text=""></asp:Label>
        <asp:Button ID="DeleteFileButton" runat="server" Text="x" BackColor="White" BorderWidth="0px" visible="false" Font-Bold="True" OnClick="DeleteFileButton_Click"/>
        <br />
&nbsp;&nbsp;
        <asp:Label ID="NoKeyLabel" runat="server" Text=""></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
        &nbsp;<asp:RadioButton ID="EncodeButton" runat="server" OnCheckedChanged="EncodeBurron_CheckedChanged" Text="Зашифровать" Checked="true" GroupName="coders" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:RadioButton ID="DecodeButton" runat="server" Text="Расшифровать" OnCheckedChanged="DecodeButton_CheckedChanged" GroupName="coders" />
        <br />
        <asp:Button ID="TranslateButton" runat="server" Text="Расчитать" OnClick="TranslateButton_Click" />
        <br />
        <p>
            <asp:TextBox ID="resultText" runat="server" style="resize:none; margin-bottom: 4px" placeholder="Здесь будет результат" TextMode="MultiLine" Height="150px" Width="600px" ReadOnly="True"></asp:TextBox>
        </p>
        <div style="margin-left: 440px">
            <asp:Button ID="LoadToFileButton" runat="server" Text="Сохранить в файл" Width="153px" OnClick="LoadToFileButton_Click" />
        </div>
    </form>
</body>
</html>
