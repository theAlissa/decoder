﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scorpion
{
    delegate int operation(int textChar, int keyChar);
       
    public partial class MainForm : System.Web.UI.Page
    {
        static operation translate;
        string path;
        static bool isTextFromFile;
        const string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        public static HttpPostedFile file;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            DeleteAllFilesFromServer();
            ChangeOperationToDecode(DecodeButton.Checked);
            isTextFromFile = !inputText.Enabled;
            path = Server.MapPath("~/Uploads");
            FileUploadButton.Attributes.Add("onclick", "document.getElementById('" + FileUploader.ClientID + "').click();return false;"); 
        }
        protected void GetFileButton_Click(object sender, EventArgs e)
        {
            file = FileUploader.PostedFile;
            ChoosenFilePath.Text = FileUploader.FileName;
            DeleteFileButton.Visible = true;
            inputText.Enabled = false;
            isTextFromFile = !inputText.Enabled;
        }
        protected void DeleteFileButton_Click(object sender, EventArgs e)
        {
            ChoosenFilePath.Text = "";
            inputText.Enabled = true;
            DeleteFileButton.Visible = false;
            isTextFromFile = !inputText.Enabled;
            DeleteFileFromServer(file.FileName);
        }
        protected void EncodeBurron_CheckedChanged(object sender, EventArgs e)
        {
            ChangeOperationToDecode(false);
        }
        protected void DecodeButton_CheckedChanged(object sender, EventArgs e)
        {
            ChangeOperationToDecode(true);
        }
        protected void TranslateButton_Click(object sender, EventArgs e)
        {
            string text = GetTextToTranslate();
            string key = inputKeyWord.Text;
            if (!isKeyValid(key))
            {
                NoKeyLabel.Text = "Недопустимый формат ключа";
            }
            else
            {
                NoKeyLabel.Text = "";
            }
            resultText.Text = Translate(text, key);
           
               
        }
        protected void LoadToFileButton_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/octect-stream";
            Response.AppendHeader("content-disposition", "filename=TranslatedFile.docx");
            Response.TransmitFile(CreateTranslatedFile());

            Response.End();
        }
        private bool checkIfValid(string text,string key)
        {
            return isKeyValid(key) & isTextValid(text) ? true : false;
        }
        public string Translate(string text, string key)
        {
            string result = "";
            int charOfText;
            int charOfKey;
            int charOfRes;
            int keyIndex = 0;
            bool isCharUpper;
            char letter;
            if (checkIfValid(text, key))
            {
                key = key.ToLower();
                for (int i = 0; i < text.Length; i++)
                {
                    isCharUpper = Char.IsUpper(text[i]) ? true : false;
                    letter = Char.ToLower(text[i]);
                    if (alphabet.Contains(letter))
                    {
                        charOfText = alphabet.IndexOf(letter);
                        charOfKey = alphabet.IndexOf(key[keyIndex]);
                        charOfRes = translate(charOfText, charOfKey);
                        
                        result +=isCharUpper?Char.ToUpper(alphabet[charOfRes]): alphabet[charOfRes];
                        keyIndex++;
                        if (keyIndex >= key.Length)
                        {
                            keyIndex = 0;
                        }
                    }
                    else
                    {
                        result += text[i];
                    }
                }
            }
            return result;
        }
        private string GetTextToTranslate()
        {
            if (isTextFromFile)
            {
                return GetTextFromFile();
            }
            else
            {
                return GetTextFromTextBox();
            }
        }
        private string GetTextFromTextBox()
        {
            return inputText.Text;
        }
        private string GetTextFromFile()
        {
            string fileExtention = GetFileExtention(file);
                if ( fileExtention!= null)
                {
                    if (!(Directory.GetFiles(Server.MapPath("~/Uploads/")).Contains(Server.MapPath("~/Uploads/")+file.FileName)))
                    {
                        DownloadFileToserver(file);
                    }
                    if (fileExtention == ".txt")
                    {
                        return SendTxtFileToTranslate(file.FileName);
                    }
                else { return SendDocxFileToTranslate(file.FileName); }
                }
                else { return null; }

        }
        public void ChangeOperationToDecode(bool isDecode)
        {
            operation decodeOperation = (x, y) => (x - y) < 0 ? alphabet.Length + (x - y) : x - y; 
            operation incodeOperation =(x, y) => (x + y) >= alphabet.Length ? x + y - alphabet.Length : x + y;
            if (isDecode)
            {
                translate = decodeOperation;
            }
            else
            {
                translate = incodeOperation;
            }
        }
        public string SendDocxFileToTranslate(string fileName)
        {
            try
            {
                WordprocessingDocument wordprocessingDocument =
                    WordprocessingDocument.Open(Server.MapPath("/Uploads/" + fileName), false);

                DocumentFormat.OpenXml.Wordprocessing.Body body =
                    wordprocessingDocument.MainDocumentPart.Document.Body;
                wordprocessingDocument.Close();
                return GetPlainText(body);
            }
            catch(Exception e)
            {
                ChoosenFilePath.Text = e.Message;
                return null;
            }
        }
        private string GetPlainText(OpenXmlElement element)
        {
            StringBuilder PlainTextInWord = new StringBuilder();
            foreach (OpenXmlElement section in element.Elements())
            {
                switch (section.LocalName)
                {
                    // Text 
                    case "t":
                        PlainTextInWord.Append(section.InnerText);
                        break;
                    case "cr":                          // Carriage return 
                    case "br":                          // Page break 
                        PlainTextInWord.Append(Environment.NewLine);
                        break;
                    // Tab 
                    case "tab":
                        PlainTextInWord.Append("\t");
                        break;
                    // Paragraph 
                    case "p":
                        PlainTextInWord.Append(GetPlainText(section));
                        PlainTextInWord.AppendLine(Environment.NewLine);
                        break;
                    default:
                        PlainTextInWord.Append(GetPlainText(section));
                        break;
                }
            }
            
            return PlainTextInWord.ToString();
        }
        public string SendTxtFileToTranslate(string fileName)
        {
            string fileText;
            string filePath = Server.MapPath("/Uploads/" + fileName);
            using (var reader = new StreamReader(filePath,GetEncoding(filePath))) 
                fileText = reader.ReadToEnd();
            return fileText;
        }
        private bool DownloadFileToserver(HttpPostedFile file)
        {
            try
            {
                    file.SaveAs(Server.MapPath("~/Uploads/" + file.FileName));
                return true;
            }
            catch (Exception)
            {
                ChoosenFilePath.Text = "Не удалось загрузить файл";
                return false;
            }
        }
        private void DeleteAllFilesFromServer()
        {
            string[] files=Directory.GetFiles(Server.MapPath("~/Uploads/"));
            foreach (var file in files)
            {
                DeleteFileFromServer(file);
            }
        }
        private void DeleteFileFromServer(string file)
        {
            try
            {
                System.IO.File.Delete(file);
            }catch(Exception e)
            {
                inputText.Text = e.Message;
            }
            
        }
        public static Encoding GetEncoding(string filename)
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe && bom[2] == 0 && bom[3] == 0) return Encoding.UTF32; 
            if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; 
            if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode;
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return new UTF32Encoding(true, true);

            return Encoding.Default;
        }
        private string GetFileExtention(HttpPostedFile file)
        {
            string ext = System.IO.Path.GetExtension(file.FileName);
            if (ext==".txt"|ext==".docx")
            {
                return ext;
            }
            else { 
                ChoosenFilePath.Text = "Поддерживаются только .docx и .txt файлы";
                return null;
            }
        }
        private bool isTextValid(string text)
        {
            return isNotNull(text);
        }
        private bool isKeyValid(string key)
        {
            bool isOk = false;
            if (isNotNull(key))
            {
                foreach (var chars in key)
                {
                    if (alphabet.Contains(chars))
                    {
                        isOk = true;
                        break;
                    }
                }
            }
            else
            {
                isOk = false;
            }
            
            return isOk;
        }
       private bool isNotNull(string s)
        {
            if (s != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private string CreateTranslatedFile()
        {
            Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
            winword.ShowAnimation = false;
            Microsoft.Office.Interop.Word.Document document = winword.Documents.Add();
            document.Content.SetRange(0, 0);
            document.Content.Text = resultText.Text;

            string filename = Server.MapPath("~/Uploads/"+"testFile.docx");
            document.SaveAs2(filename);
            document.Close();
            return filename;
        }
        
    }
}