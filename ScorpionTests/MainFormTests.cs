﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scorpion;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Scorpion.Tests
{
    [TestClass()]
    public class MainFormEncodeTests
    {

        [TestMethod()]
        public void TranslateNormalTextAndKey()
        {
            string text = "Что мне нужно для счастья?";
            string key = "котейка";
            string expected = "Вбб счп нюхау нця ьётцьжя?";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(false);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void TranslateEmptyTextAndKey()
        {
            string text = "";
            string key = "";
            string expected = "";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(false);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TranslateEmptyText()
        {
            string text = "";
            string key = "котейка";
            string expected = "";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(false);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void TranslateEmptyKey()
        {
            string text = "котейка";
            string key = "";
            string expected = "";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(false);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void TranslateWrongAlphabetKey()
        {
            string text = "котейка";
            string key = "asdå";
            string expected = "";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(false);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TranslateWrongAlphabetText()
        {
            string text = "To be or_not to be?";
            string key = "котейка";
            string expected = "To be or_not to be?";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(false);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }

    }
    [TestClass()]
    public class MainFormDecodeTests
    {


        [TestMethod()]
        public void TranslateNormalTextAndKey()
        {
            string text = "Вбб счп нюхау нця ьётцьжя?";
            string key = "котейка";
            string expected = "Что мне нужно для счастья?";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(true);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void TranslateEmptyTextAndKey()
        {
            string text = "";
            string key = "";
            string expected = "";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(true);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TranslateEmptyText()
        {
            string text = "";
            string key = "котейка";
            string expected = "";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(true);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void TranslateEmptyKey()
        {
            string text = "котейка";
            string key = "";
            string expected = "";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(true);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void TranslateWrongAlphabetKey()
        {
            string text = "котейка";
            string key = "asdå";
            string expected = "";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(true);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TranslateWrongAlphabetText()
        {
            string text = "To be or_not to be?";
            string key = "котейка";
            string expected = "To be or_not to be?";

            MainForm mainForm = new MainForm();
            mainForm.ChangeOperationToDecode(true);
            string actual = mainForm.Translate(text, key);

            Assert.AreEqual(expected, actual);
        }

    }
}